﻿using Orders.Domain.Models;

namespace Orders.Domain.Dtos;

public class ItemDto
{
    public string ArticleNumber { get; set; }
    public int Quantity { get; set; }

    public Item ToModel() =>
        new()
        {
            Quantity = Quantity,
            ArticleNumber = ArticleNumber
            
        };
}