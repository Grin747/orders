﻿using Orders.Domain.Models.Sources;

namespace Orders.Domain.Models;

public class Client
{
    public int Id { get; set; }
    public string Name { get; set; }
    
    public ICollection<Order> Orders { get; set; }
    public ICollection<Source> Sources { get; set; }
}