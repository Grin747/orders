﻿namespace Orders.Domain.Models;

public class Item
{
    public int Id { get; set; }
    public string ArticleNumber { get; set; }
    public int Quantity { get; set; }

    public int OrderId { get; set; }
    public Order Order { get; set; }
}