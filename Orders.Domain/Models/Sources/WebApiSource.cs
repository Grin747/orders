﻿namespace Orders.Domain.Models.Sources;

public class WebApiSource : Source
{
    public Uri Uri { get; set; }
}