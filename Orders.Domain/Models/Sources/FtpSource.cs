﻿namespace Orders.Domain.Models.Sources;

public class FtpSource : Source
{
    public Uri Uri { get; set; }
    public string Login { get; set; }
    public string Password { get; set; }
}