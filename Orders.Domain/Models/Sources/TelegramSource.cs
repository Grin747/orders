﻿namespace Orders.Domain.Models.Sources;

public class TelegramSource : Source
{
    public string Username { get; set; }
}