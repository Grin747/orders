﻿namespace Orders.Domain.Models.Sources;

public class EmailSource : Source
{
    public string Email { get; set; }
}