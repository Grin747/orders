﻿namespace Orders.Domain.Models.Sources;

public abstract class Source
{
    public int Id { get; set; }

    public int ClientId { get; set; }
    public Client Client { get; set; }
}