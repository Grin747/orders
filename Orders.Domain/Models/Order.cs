﻿namespace Orders.Domain.Models;

public class Order
{
    public int Id { get; set; }
    public DateTime Ordered { get; set; }

    public int? ClientId { get; set; }
    public Client? Client { get; set; }

    public ICollection<Item> Items { get; set; }
}