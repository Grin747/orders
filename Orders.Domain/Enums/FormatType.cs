﻿namespace Orders.Domain.Enums;

public enum FormatType
{
    Json,
    Xlsx,
    Csv,
    Xml
}