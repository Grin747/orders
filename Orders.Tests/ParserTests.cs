﻿using System.Text;
using NUnit.Framework;
using Orders.Domain.Dtos;
using Orders.Domain.Enums;
using Orders.Infrastructure.Parsers;

namespace Orders.Tests;

public class ParserTests
{
    [Test]
    public void JsonParsing_ValidData()
    {
        var parser = ParserFactory.GetParser(FormatType.Json);
        var content = @"[
    {
        ""ArticleNumber"":""00047b012"",
        ""Quantity"":23
    }
]";

        var stream = new MemoryStream(Encoding.UTF8.GetBytes(content));

        var items = parser.Parse(stream);
        
        Assert.IsNotEmpty(items);
        
        var expected =
            new ItemDto
            {
                Quantity = 23,
                ArticleNumber = "00047b012"
            };

        var actual = items.First();

        Assert.AreEqual(expected.Quantity, actual.Quantity);
        Assert.AreEqual(expected.ArticleNumber, actual.ArticleNumber);
    }
}