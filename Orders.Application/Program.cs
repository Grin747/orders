using Orders.Application;
using Orders.Infrastructure.Trackers;

var host = Host.CreateDefaultBuilder(args)
    .ConfigureServices(services =>
    {
        // Добавить контекст БД
        services.AddTrackers();
        services.AddHostedService<Worker>();
    })
    .Build();

await host.RunAsync();