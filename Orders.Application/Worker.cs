using Orders.Domain.Models;
using Orders.Infrastructure;
using Orders.Infrastructure.Database;
using Orders.Infrastructure.Parsers;
using Orders.Infrastructure.Trackers;

namespace Orders.Application;

public class Worker : IHostedService
{
    private readonly ILogger<Worker> _logger;
    private readonly IServiceProvider _provider;

    public Worker(ILogger<Worker> logger, IServiceProvider provider)
    {
        _logger = logger;
        _provider = provider;
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("Service starting");
        
        var services = _provider.GetServices<ITracker>();

        foreach (var service in services)
        {
            service.StartTracking(Callback);
        }
        
        _logger.LogInformation("Service started");
        
        return Task.CompletedTask;
    }

    private void Callback(Stream file, string fileName, Client? client)
    {
        var parser = ParserFactory.GetParser(FormatResolver.ResolveFromFilename(fileName));
        var items = parser.Parse(file);
        using var db = _provider.GetRequiredService<OrdersContext>();
        
        var order = new Order
        {
            Items = items.Select(x => x.ToModel()).ToList(),
            Client = client,
            Ordered = DateTime.UtcNow
        };

        db.Orders.Add(order);
        db.SaveChanges();
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }
}