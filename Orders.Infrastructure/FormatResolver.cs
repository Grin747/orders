﻿using Orders.Domain.Enums;

namespace Orders.Infrastructure;

public static class FormatResolver
{
    public static FormatType ResolveFromFilename(string filename)
    {
        var ext = filename.Split('.').Last();

        return ext switch
        {
            "xlsx" => FormatType.Xlsx,
            "csv" => FormatType.Csv,
            "json" => FormatType.Json,
            "xml" => FormatType.Xml,
            _ => throw new ArgumentOutOfRangeException(nameof(filename),
                $"There is no parser for {ext}")
        };
    }
}