﻿using Microsoft.EntityFrameworkCore;
using Orders.Domain.Models;
using Orders.Domain.Models.Sources;

namespace Orders.Infrastructure.Database;

public class OrdersContext : DbContext
{
    public DbSet<Client> Clients { get; set; }
    public DbSet<Order> Orders { get; set; }
    public DbSet<Source> Sources { get; set; }
}