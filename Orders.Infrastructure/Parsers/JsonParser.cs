﻿using Newtonsoft.Json;
using Orders.Domain.Dtos;

namespace Orders.Infrastructure.Parsers;

internal class JsonParser : IParser
{
    public ICollection<ItemDto> Parse(Stream content)
    {
        var reader = new StreamReader(content);
        var text = reader.ReadToEnd();
        return JsonConvert.DeserializeObject<ICollection<ItemDto>>(text);
    }
}