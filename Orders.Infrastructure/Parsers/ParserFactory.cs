﻿using Orders.Domain.Enums;

namespace Orders.Infrastructure.Parsers;

public static class ParserFactory
{
    public static IParser GetParser(FormatType type) =>
        type switch
        {
            FormatType.Json => new JsonParser(),
            _ => throw new ArgumentOutOfRangeException(nameof(type), $"No parser for type {type}")
        };
}