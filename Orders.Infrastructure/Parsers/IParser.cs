﻿using Orders.Domain.Dtos;

namespace Orders.Infrastructure.Parsers;

public interface IParser
{
    /// <summary>
    /// Парсит файл с заказом.
    /// </summary>
    /// <param name="content">
    /// Файл, который нужно распарсить.
    /// </param>
    /// <returns>
    /// Результат парсинга - заказ.
    /// </returns>
    ICollection<ItemDto> Parse(Stream content);
}