﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Orders.Domain.Models;
using Orders.Domain.Models.Sources;
using Orders.Infrastructure.Database;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace Orders.Infrastructure.Trackers;

internal class TelegramTracker : ITracker
{
    private readonly ITelegramBotClient _bot;
    private readonly IServiceProvider _provider;
    private readonly ILogger<TelegramTracker> _logger;
    private Action<Stream, string, Client?> _callback;

    public TelegramTracker(IServiceProvider provider, ILogger<TelegramTracker> logger)
    {
        _provider = provider;
        _logger = logger;
        //Токен можно хранить в appsettings
        _bot = new TelegramBotClient("");
    }
    
    public void StartTracking(Action<Stream, string, Client?> callback)
    {
        _callback = callback;
        _bot.StartReceiving(HandleUpdateAsync, HandleErrorAsync);
    }

    private async Task HandleUpdateAsync(ITelegramBotClient botClient,
        Update update, CancellationToken cancellationToken)
    {
        Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(update));
        if(update.Type == Telegram.Bot.Types.Enums.UpdateType.Message &&
           update.Message?.Document?.FileName != null)
        {
            var message = update.Message;
            var fname = message.Document.FileName;
            var uname = message.From?.Username;
            var content = Stream.Null; //тут нужно скачать файл через апи телеграма
            var client = new Client();

            if (!string.IsNullOrEmpty(uname))
            {
                await using var db = _provider.GetRequiredService<OrdersContext>();
                client = await db.Sources
                    .OfType<TelegramSource>()
                    .Where(x => x.Username == uname)
                    .Select(x => x.Client)
                    .FirstOrDefaultAsync(cancellationToken);
            }

            _callback(content, fname, client);
        }
    }

    private async Task HandleErrorAsync(ITelegramBotClient botClient,
        Exception exception, CancellationToken cancellationToken)
    {
        _logger.LogError("Error during telegram tracking {Message}", exception.Message);
    }
}