﻿using Orders.Domain.Models;
using Orders.Domain.Models.Sources;

namespace Orders.Infrastructure.Trackers;

public interface ITracker
{
    /// <summary>
    /// Заставляет трекер начать следить за выбранным источником T
    /// </summary>
    /// <param name="callback">
    /// Действие, которое выполнится при обнаружении новой задачи.
    /// Первый аргумент - файл, который нужно распарсить;
    /// второй - имя файла;
    /// третий - клиент, совершивший заказ.
    /// </param>
    void StartTracking(Action<Stream, string, Client?> callback);
}