﻿using Microsoft.Extensions.DependencyInjection;

namespace Orders.Infrastructure.Trackers;

public static class TrackersExtensions
{
    public static IServiceCollection AddTrackers(this IServiceCollection services) =>
        services.AddSingleton<ITracker, TelegramTracker>()
            .AddSingleton<ITracker, FtpTracker>();
}